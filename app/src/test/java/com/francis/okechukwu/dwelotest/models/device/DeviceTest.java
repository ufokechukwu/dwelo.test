package com.francis.okechukwu.dwelotest.models.device;

import org.junit.Test;

import java.util.UUID;

import javax.xml.validation.Validator;

import static org.junit.Assert.*;

public class DeviceTest {
    String uuid = UUID.randomUUID().toString().toUpperCase();
    String name = "device-1";
    Device device = new Device(uuid, name, Device.Type.SWITCH);

    @Test
    public void getId() {
        assertEquals(uuid, device.getId());
    }

    @Test
    public void getName() {
        assertEquals(name, device.getName());
    }

    @Test
    public void setName() {
        device.setName("device-1-2");
        assertEquals("device-1-2", device.getName());
    }

    @Test
    public void getType() {
        assertEquals(Device.Type.SWITCH, device.getType());
    }

    @Test
    public void setDeviceId() {
        String uuidNew = UUID.randomUUID().toString().toUpperCase();
        device.setDeviceId(uuidNew);
        assertEquals(uuidNew, device.getId());
    }
}