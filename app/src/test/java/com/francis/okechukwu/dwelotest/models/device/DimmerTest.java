package com.francis.okechukwu.dwelotest.models.device;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class DimmerTest {
    String uuid = UUID.randomUUID().toString().toUpperCase();
    String name = "device-1";
    Double level = 50.0;
    Device device = new Dimmer(uuid, name, level);

    @Test
    public void getLevel() {
        Double levelValue = ((Dimmer)device).getLevel();
        assertEquals(level, levelValue);
    }

    @Test
    public void getType() {
        assertEquals(device.getType(), Device.Type.DIMMER);
    }
}