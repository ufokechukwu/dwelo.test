package com.francis.okechukwu.dwelotest.models.install_info;

import java.util.List;

public class DeviceStateInfo {

    private String deviceId;
    private List<DeviceStatus> deviceStatuses;

    public DeviceStateInfo(String deviceId, List<DeviceStatus> deviceStatuses) {
        this.deviceId = deviceId;
        this.deviceStatuses = deviceStatuses;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<DeviceStatus> getDeviceStatuses() {
        return deviceStatuses;
    }

    public void setDeviceStatuses(List<DeviceStatus> deviceStatuses) {
        this.deviceStatuses = deviceStatuses;
    }

    public DeviceStatus getFinalStatus(){
        DeviceStatus status = new DeviceStatus("", DeviceStatus.State.REQUESTED);
        if (deviceStatuses.size() > 0){
            status = deviceStatuses.get(deviceStatuses.size() - 1);
        }
        return status;
    }
}
