package com.francis.okechukwu.dwelotest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;
import com.francis.okechukwu.dwelotest.helpers.UIHelper;
import com.francis.okechukwu.dwelotest.interfaces.AdapterActionListener;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InstallExpandListAdapter extends BaseExpandableListAdapter {
    private List<InstallGroup> groups;
    private DevicesInfoDao devicesInfoDao;
    private DevicesDao devicesDao;
    private List<List<Device>>  groupedDevices;
    final public static String EXPAND = "EXPAND";
    final public static String CLICK_ACTION = "CLICK_ACTION";

    public InstallExpandListAdapter(List<InstallGroup> groups) {
        this.groups = groups;
        this.devicesInfoDao = DAO.getDevicesInfoDaoInstance();
        this.devicesDao = DAO.getDevicesDaoInstance();
        this.groupedDevices = getGroupedDevicesInfo();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return groupedDevices.get(i).size();
    }

    @Override
    public Object getGroup(int i) {
        return groups.get(i);
    }

    @Override
    public Object getChild(int i, int childIndex) {
        return groupedDevices.get(i).get(childIndex);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int childIndex) {
        return childIndex;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        InstallGroup group = (InstallGroup) getGroup(i);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.install_status_group_cell, null);
        }

        TextView stateNameTextView = view.findViewById(R.id.stateNameTextView);
        TextView stateDescriptionTextView = view.findViewById(R.id.stateDescriptionTextView);
        ImageView stateIconImageView = view.findViewById(R.id.stateIconImageView);

        stateNameTextView.setText(group.getState().toString().substring(0, 1).toUpperCase() + group.getState().toString().substring(1).toLowerCase());

        int childrenCount = getChildrenCount(i);
        stateDescriptionTextView.setText(childrenCount == 1? childrenCount + " device" : childrenCount + " devices");
        stateIconImageView.setImageResource(group.getResourceId());

        return view;
    }

    @Override
    public View getChildView(int i, int childIndex, boolean b, View convertView, ViewGroup viewGroup) {
        Device currentDevice = (Device) getChild(i, childIndex);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.install_status_item_cell, null);
        }

        TextView deviceNameTextView = convertView.findViewById(R.id.deviceNameTextView);
        TextView stateDateTextView = convertView.findViewById(R.id.stateDateTextView);
        TextView stateCommentTextView = convertView.findViewById(R.id.stateCommentTextView);
        ImageView deviceIconImageView = convertView.findViewById(R.id.deviceIconImageView);
        ImageButton switchStateImageButton = convertView.findViewById(R.id.switchStateImageButton);

        deviceNameTextView.setText(currentDevice.getName());
        DeviceStatus finalDeviceStatus = devicesInfoDao.getDeviceInfoById(currentDevice.getId()).getFinalStatus();
        stateDateTextView.setText(finalDeviceStatus.getDate());
        stateCommentTextView.setText(finalDeviceStatus.getComment());
        deviceIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(currentDevice));

        convertView.setOnClickListener(view -> action.run(view, i, childIndex, CLICK_ACTION, currentDevice.getId()));

        switchStateImageButton.setOnClickListener(view -> action.run(view, i, childIndex, CLICK_ACTION, currentDevice.getId()));


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int childIndex) {
        return true;
    }


    // Set Custom Listeners...
    private AdapterActionListener action;
    public void setActionListener(AdapterActionListener action){
        this.action = action;
    }


    public List<List<Device>> getGroupedDevicesInfo(){
        HashMap<String, List<Device>> mStateDeviceMap = new HashMap<>();

        List<Device> mAllDevices = this.devicesDao.getAllDevices();
        for (Device device: mAllDevices){
            String mFinalState = devicesInfoDao.getDeviceInfoById(device.getId()).getFinalStatus().getState().toString();
            List<Device> mStateDeviceList = mStateDeviceMap.get(mFinalState) == null ? new ArrayList<>(): mStateDeviceMap.get(mFinalState);
            mStateDeviceList.add(device);
            mStateDeviceMap.put(mFinalState, mStateDeviceList);
        }

        List<List<Device>> mappedDevicesList = new ArrayList<>();
        for (InstallGroup group: groups){
            List<Device> mDevicesList = mStateDeviceMap.get(group.getState().toString());
            mDevicesList = mDevicesList == null ? new ArrayList<>() : mDevicesList;
            mappedDevicesList.add(mDevicesList);
        }

        return mappedDevicesList;
    }
}
