package com.francis.okechukwu.dwelotest.dao;

public class DataAPI {
    public static String getAPIDataString(){
        String apiDataString = "{\n" +
                "  \"devices\": [\n" +
                "    { \"name\": \"light switch 1\", \"type\": \"SWITCH\", \"state\": \"off\" },\n" +
                "    { \"name\": \"light switch 2\", \"type\": \"SWITCH\", \"state\": \"off\" },\n" +
                "    { \"name\": \"light switch 3\", \"type\": \"SWITCH\", \"state\": \"off\" },\n" +
                "    { \"name\": \"light switch 4\", \"type\": \"SWITCH\", \"state\": \"off\" },\n" +
                "    { \"name\": \"door lock\", \"type\": \"LOCK\", \"codes\": [ \"1234\", \"2345\", \"3456\", \"4567\" ], \"locked\": true },\n" +
                "    { \"name\": \"hallway dimmer\", \"type\": \"DIMMER\", \"level\": 0.85 },\n" +
                "    { \"name\": \"hallway dimmer\", \"type\": \"DIMMER\", \"level\": 0.85 },\n" +
                "    { \"name\": \"thermostat\", \"type\": \"THERMO\", \"temp\": 72.0, \"mode\": \"AUTO\" }\n" +
                "  ]\n" +
                "}";

        return apiDataString;
    }

}
