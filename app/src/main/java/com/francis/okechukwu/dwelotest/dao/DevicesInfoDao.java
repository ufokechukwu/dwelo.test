package com.francis.okechukwu.dwelotest.dao;

import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;

import java.util.List;

public interface DevicesInfoDao {
    List<DeviceStateInfo> getAllDeviceInfo();
    DeviceStateInfo getDeviceInfoById(String id);
    void addUpdateDeviceInfo(DeviceStateInfo deviceInfo);
    void deleteDeviceInfo(String id);
}
