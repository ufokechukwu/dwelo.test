package com.francis.okechukwu.dwelotest.dao;

import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class DevicesDaoImpl implements DevicesDao {

    private HashMap<String, Device> devices = new HashMap<>();

    @Override
    public List<Device> getAllDevices() {
        List<Device> sortedDevices = devices.values().stream().sorted(Comparator.comparing(Device::getName)).collect(Collectors.toList());
        return sortedDevices;
    }

    @Override
    public List<Device> getAllDevices(Device.Type type) {
        List<Device> selectedDevices = new ArrayList<>();
        List<Device> sortedDevices = devices.values().stream().sorted(Comparator.comparing(Device::getName)).collect(Collectors.toList());

        for (Device device : sortedDevices){
            if(device.getType().equals(type)){
                selectedDevices.add(device);
            }
        }

        return selectedDevices;
    }

    @Override
    public Device getDevice(String id) {
        return devices.get(id);
    }

    @Override
    public void addUpdateDevice(Device device) {
        devices.put(device.getId(), device);
    }

    @Override
    public void deleteDevice(String id) {
        devices.remove(id);
    }

}
