package com.francis.okechukwu.dwelotest.ui.activities.create_edit_device;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.constants.AppConstants;
import com.francis.okechukwu.dwelotest.databinding.ActivityCreateEditDeviceBinding;
import com.francis.okechukwu.dwelotest.helpers.UIHelper;
import com.francis.okechukwu.dwelotest.interfaces.ViewSetupInterface;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CreateEditDeviceActivity extends AppCompatActivity implements ViewSetupInterface {
    private ActivityCreateEditDeviceBinding binding;
    private CreateEditDeviceActivityViewModel createEditDeviceActivityViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCreateEditDeviceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle("REQUEST NEW DEVICE");
        initViewSetupInterfaceMethods();    //initViewModels() -> initViews() -> initListeners() -> initViewLoaded()

    }

    @Override
    public void initViewModels() {
        createEditDeviceActivityViewModel = new ViewModelProvider(this).get(CreateEditDeviceActivityViewModel.class);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            createEditDeviceActivityViewModel.setViewType(extras.getString(AppConstants.CREATE_EDIT_ACTIVITY));
            binding.deviceStatusStateTextLayout.setEnabled(false);
            if(createEditDeviceActivityViewModel.isEditMode()){
                String deviceId = extras.getString(AppConstants.DEVICE_ID);
                Device device = createEditDeviceActivityViewModel.getDevices().getDevice(deviceId);
                createEditDeviceActivityViewModel.setDeviceId(deviceId);
                binding.deviceStatusStateTextLayout.setEnabled(true);
                binding.deviceTypeAutoCompleteTextView.setText(device.getType().toString());
                binding.deviceIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(device.getType().toString()));
                binding.deviceTypeTextLayout.setEnabled(false);
                binding.deviceNameEditText.setEnabled(false);
                binding.deviceNameEditText.setText(device.getName());
                setTitle("UPDATE DEVICE STATUS");
            }

        }
    }

    @Override
    public void initViews() {
        setDeviceTypeDropDown();
        setDeviceStatusStateDropDown();
    }

    private void setDeviceStatusStateDropDown() {
        List<String> deviceStatusDropDownItems = Arrays.asList(
                DeviceStatus.State.REQUESTED.toString(),
                DeviceStatus.State.PURCHASED.toString(),
                DeviceStatus.State.SHIPPED.toString(),
                DeviceStatus.State.INSTALLED.toString());

        if(createEditDeviceActivityViewModel.isEditMode()){
            String deviceId = createEditDeviceActivityViewModel.getDeviceId();
            DeviceStateInfo deviceStateInfo = createEditDeviceActivityViewModel.getDevicesInfo().getDeviceInfoById(deviceId);
            int index = deviceStatusDropDownItems.indexOf(deviceStateInfo.getFinalStatus().getState().toString());
            deviceStatusDropDownItems = deviceStatusDropDownItems.subList(index + 1, deviceStatusDropDownItems.size());
        }

        ArrayAdapter<String> statusArrayAdapter = new ArrayAdapter<>(this,
                R.layout.simple_list_cell, R.id.itemTextView, deviceStatusDropDownItems);
        binding.deviceStatusStateAutoCompleteTextView.setAdapter(statusArrayAdapter);

    }

    private void setDeviceTypeDropDown() {
        List<String> deviceTypeDropDownItems = Arrays.asList(
                Device.Type.SWITCH.toString(),
                Device.Type.DIMMER.toString(),
                Device.Type.THERMO.toString(),
                Device.Type.LOCK.toString());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.simple_list_cell, R.id.itemTextView, deviceTypeDropDownItems);
        binding.deviceTypeAutoCompleteTextView.setAdapter(arrayAdapter);
    }

    @Override
    public void initListeners() {
        binding.deviceTypeAutoCompleteTextView.addTextChangedListener(getTextChangeListener());
        binding.deviceSaveMaterialButton.setOnClickListener(onSaveButtonClickListener());
    }

    @Override
    public void initViewLoaded() {

        String deviceId = createEditDeviceActivityViewModel.getDeviceId();
        DeviceStateInfo deviceStateInfo = createEditDeviceActivityViewModel.getDevicesInfo().getDeviceInfoById(deviceId);

        if(createEditDeviceActivityViewModel.isEditMode() && deviceStateInfo.getFinalStatus().getState().toString().equals(DeviceStatus.State.INSTALLED.toString())){
            // Device is INSTALLED
            binding.deviceStatusStateAutoCompleteTextView.setText(DeviceStatus.State.INSTALLED.toString());
            binding.deviceStatusStateTextLayout.setEnabled(false);

            binding.deviceCommentsEditText.setText(deviceStateInfo.getFinalStatus().getComment());
            binding.deviceCommentsEditText.setEnabled(false);

            binding.deviceSaveMaterialButton.setEnabled(false);

            setTitle("INSTALLED DEVICE");
        }else if (createEditDeviceActivityViewModel.isCreateMode()){
            binding.deviceStatusStateAutoCompleteTextView.setText(DeviceStatus.State.REQUESTED.toString());
            binding.deviceStatusStateTextLayout.setEnabled(false);
        }
    }



    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @NonNull
    private View.OnClickListener onSaveButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                Boolean hasName = binding.deviceNameEditText.getText().toString().trim().length() > 0;
                Boolean hasType = binding.deviceTypeAutoCompleteTextView.getText().toString().trim().length() > 0;
                Boolean hasStatus = binding.deviceStatusStateAutoCompleteTextView.getText().toString().trim().length() > 0;
                Boolean hasComments = binding.deviceCommentsEditText.getText().toString().trim().length() > 0;
                if (hasName && hasType && hasStatus && hasComments) {
                    createDeviceData();
                    // dismiss activity...
                    finish();
                } else {
                    showErrorMessage(hasName, hasType, hasStatus, hasComments);
                }
            }
        };
    }

    private void createDeviceData() {
        Device device;
        String deviceName = binding.deviceNameEditText.getText().toString();
        String deviceState = binding.deviceStatusStateAutoCompleteTextView.getText().toString();
        String deviceType = binding.deviceTypeAutoCompleteTextView.getText().toString();
        String deviceComment = binding.deviceCommentsEditText.getText().toString();

        if(createEditDeviceActivityViewModel.isCreateMode()){
            device = createEditDeviceActivityViewModel.createDefaultDeviceOfType(deviceType, deviceName);
            DeviceStatus deviceStatus = new DeviceStatus(deviceComment, deviceState);
            List<DeviceStatus> statuses = new ArrayList<>(Arrays.asList(deviceStatus));
            DeviceStateInfo deviceStateInfo = new DeviceStateInfo(device.getId(), statuses);
            createEditDeviceActivityViewModel.getDevicesInfo().addUpdateDeviceInfo(deviceStateInfo);
            createEditDeviceActivityViewModel.getDevices().addUpdateDevice(device);
        }else{
            String deviceId = createEditDeviceActivityViewModel.getDeviceId();
            // device = createEditDeviceActivityViewModel.getDevices().getDevice(deviceId);
            DeviceStatus deviceStatus = new DeviceStatus(deviceComment, deviceState);
            DeviceStateInfo deviceStateInfo = createEditDeviceActivityViewModel.getDevicesInfo().getDeviceInfoById(deviceId);
            List<DeviceStatus> statuses = deviceStateInfo.getDeviceStatuses();
            statuses.add(deviceStatus);
            deviceStateInfo.setDeviceStatuses(statuses);
            createEditDeviceActivityViewModel.getDevicesInfo().addUpdateDeviceInfo(deviceStateInfo);
        }
    }

    private void showErrorMessage(Boolean hasName, Boolean hasType, Boolean hasStatus, Boolean hasComments) {
        String errorVariables = "[ ";
        errorVariables = hasName ? errorVariables: errorVariables + "-NAME";
        errorVariables = hasType ? errorVariables: errorVariables + "-TYPE";
        errorVariables = hasStatus ? errorVariables: errorVariables + "-STATUS";
        errorVariables = hasComments ? errorVariables: errorVariables + "-COMMENTS";
        errorVariables = errorVariables + " -]";

        new AlertDialog.Builder(CreateEditDeviceActivity.this)
                .setTitle("Error")
                .setMessage("One or more fields are missing "+ errorVariables+". All fields are required.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    private TextWatcher getTextChangeListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                binding.deviceIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(editable.toString()));
            }
        };
    }
}