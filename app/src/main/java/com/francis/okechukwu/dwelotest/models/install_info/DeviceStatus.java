package com.francis.okechukwu.dwelotest.models.install_info;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DeviceStatus {
    private String date;
    private String comment;
    private State state;

    public DeviceStatus(String comment, State state) {
        this.comment = comment;
        this.state = state;
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy  HH:mm:ss");
        this.date = formatter.format(Calendar.getInstance().getTime());
    }

    public DeviceStatus(String comment, String stateString) {
        this.comment = comment;
        this.state = getStateFromString(stateString);
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy  HH:mm:ss");
        this.date = formatter.format(Calendar.getInstance().getTime());
    }

    public String getDate() {
        return date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


    public static State getStateFromString(String stateString){
        if (stateString.equals(State.REQUESTED.toString())){
            return State.REQUESTED;
        }else if (stateString.equals(State.PURCHASED.toString())){
            return State.PURCHASED;
        }else if (stateString.equals(State.SHIPPED.toString())){
            return State.SHIPPED;
        }else if (stateString.equals(State.INSTALLED.toString())){
            return State.INSTALLED;
        }
        return State.REQUESTED;
    }


    public enum State {
        REQUESTED {
            public String toString() {
                return "REQUESTED";
            }
        },
        PURCHASED {
            public String toString() {
                return "PURCHASED";
            }
        },
        SHIPPED {
            public String toString() {
                return "SHIPPED";
            }
        },
        INSTALLED {
            public String toString() {
                return "INSTALLED";
            }
        }
    }
}
