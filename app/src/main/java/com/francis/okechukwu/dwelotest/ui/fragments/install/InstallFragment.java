package com.francis.okechukwu.dwelotest.ui.fragments.install;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.adapters.InstallExpandListAdapter;
import com.francis.okechukwu.dwelotest.constants.AppConstants;
import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;
import com.francis.okechukwu.dwelotest.databinding.FragmentInstallBinding;
import com.francis.okechukwu.dwelotest.interfaces.AdapterActionListener;
import com.francis.okechukwu.dwelotest.interfaces.ViewSetupInterface;
import com.francis.okechukwu.dwelotest.models.device.Switch;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;
import com.francis.okechukwu.dwelotest.ui.activities.create_edit_device.CreateEditDeviceActivity;
import com.francis.okechukwu.dwelotest.ui.activities.device_status_detail.DeviceStatusDetailActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class InstallFragment extends Fragment implements ViewSetupInterface {

    private InstallViewModel installViewModel;
    private FragmentInstallBinding binding;
    private InstallExpandListAdapter installExpandListAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentInstallBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewSetupInterfaceMethods();    //initViewModels() -> initViews() -> initListeners() -> initViewLoaded()
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void initViewModels() {
        installViewModel = new ViewModelProvider(this).get(InstallViewModel.class);
    }

    @Override
    public void initViews() {
        reloadListView();
    }

    @Override
    public void initListeners() {
        setListViewListener();

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateEditDeviceActivity.class);
                intent.putExtra(AppConstants.CREATE_EDIT_ACTIVITY , AppConstants.CREATE);
                startActivity(intent);
            }
        });


    }

    public void reloadListView(){
        List<InstallGroup> groups = new ArrayList<>();
        groups.add(new InstallGroup(DeviceStatus.State.REQUESTED, R.drawable.request_24));
        groups.add(new InstallGroup(DeviceStatus.State.PURCHASED, R.drawable.purchased_paid_24));
        groups.add(new InstallGroup(DeviceStatus.State.SHIPPED, R.drawable.baseline_local_shipping_24));
        groups.add(new InstallGroup(DeviceStatus.State.INSTALLED, R.drawable.baseline_check_circle_24));
        installExpandListAdapter = new InstallExpandListAdapter(groups);
        binding.statusExpandableListView.setAdapter(installExpandListAdapter);
    }

    public void setListViewListener(){
        installExpandListAdapter.setActionListener(new AdapterActionListener() {
            @Override
            public void run(View v, int groupPosition, int childPosition, String tag, Object data) {
                if (tag != null && tag.equals(InstallExpandListAdapter.CLICK_ACTION)){

                    if (v.getId() == R.id.switchStateImageButton){
                        Intent intent = new Intent(getActivity(), CreateEditDeviceActivity.class);
                        intent.putExtra(AppConstants.CREATE_EDIT_ACTIVITY , AppConstants.EDIT);
                        String deviceId = (String) data;
                        intent.putExtra(AppConstants.DEVICE_ID , deviceId);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(getActivity(), DeviceStatusDetailActivity.class);
                        String deviceId = (String) data;
                        intent.putExtra(AppConstants.DEVICE_ID , deviceId);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void initViewLoaded() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (installExpandListAdapter != null){
            reloadListView();
            setListViewListener();
        }
    }
}