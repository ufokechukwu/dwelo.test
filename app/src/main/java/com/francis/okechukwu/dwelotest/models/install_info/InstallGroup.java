package com.francis.okechukwu.dwelotest.models.install_info;

public class InstallGroup {
    private DeviceStatus.State state;
    private int resourceId;

    public InstallGroup(DeviceStatus.State state, int resourceId) {
        this.state = state;
        this.resourceId = resourceId;
    }

    public DeviceStatus.State getState() {
        return state;
    }

    public void setState(DeviceStatus.State state) {
        this.state = state;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

}
