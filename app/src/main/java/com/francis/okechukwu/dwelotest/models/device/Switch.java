package com.francis.okechukwu.dwelotest.models.device;

public class Switch extends Device{
    private String state;

    public Switch(String id, String name, String state) {
        super(id, name, Type.SWITCH);
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
