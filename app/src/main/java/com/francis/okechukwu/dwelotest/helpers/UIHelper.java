package com.francis.okechukwu.dwelotest.helpers;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;

public class UIHelper {

    public static int getDeviceTypeResourceId(Device device){
        if (device.getType().toString().equals(Device.Type.SWITCH.toString())){
            return R.drawable.switch_lightbulb_24;
        }else if (device.getType().toString().equals(Device.Type.THERMO.toString())){
            return R.drawable.thermostat_24;
        }else if (device.getType().toString().equals(Device.Type.DIMMER.toString())){
            return R.drawable.dimmer_24;
        }else if (device.getType().toString().equals(Device.Type.LOCK.toString())){
            return R.drawable.lock_key_24;
        }

        return R.drawable.switch_lightbulb_24;
    }

    public static int getDeviceTypeResourceId(String type){
        if (type.equals(Device.Type.SWITCH.toString())){
            return R.drawable.switch_lightbulb_24;
        }else if (type.equals(Device.Type.THERMO.toString())){
            return R.drawable.thermostat_24;
        }else if (type.equals(Device.Type.DIMMER.toString())){
            return R.drawable.dimmer_24;
        }else if (type.toString().equals(Device.Type.LOCK.toString())){
            return R.drawable.lock_key_24;
        }

        return R.drawable.switch_lightbulb_24;
    }

    public static int getStateResourceId(DeviceStatus.State state){
        int id = R.drawable.request_24;

        if (state.toString().equals(DeviceStatus.State.REQUESTED.toString())){
            id = R.drawable.request_24;
        }else if (state.toString().equals(DeviceStatus.State.PURCHASED.toString())){
            id = R.drawable.purchased_paid_24;
        }else if (state.toString().equals(DeviceStatus.State.SHIPPED.toString())){
            id = R.drawable.baseline_local_shipping_24;
        }else if (state.toString().equals(DeviceStatus.State.INSTALLED.toString())){
            id = R.drawable.baseline_check_circle_24;
        }

        return id;
    }
}
