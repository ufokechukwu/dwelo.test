package com.francis.okechukwu.dwelotest.models.device;

public class Thermostat extends Device {
    private double temp;
    private Mode mode;


    public Thermostat(String id, String name, Double temp, Mode mode) {
        super(id, name, Type.THERMO);
        this.temp = temp;
        this.mode = mode;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }


    public enum Mode {
        MANUAL {
            public String toString() {
                return "MANUAL";
            }
        },

        AUTO {
            public String toString() {
                return "AUTO";
            }
        }
    }
}
