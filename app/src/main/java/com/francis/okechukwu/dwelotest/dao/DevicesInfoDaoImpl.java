package com.francis.okechukwu.dwelotest.dao;

import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class DevicesInfoDaoImpl implements DevicesInfoDao {

    private HashMap<String, DeviceStateInfo> devicesInfo = new HashMap<>();

    @Override
    public List<DeviceStateInfo> getAllDeviceInfo() {
        return new ArrayList<>(devicesInfo.values());
    }

    @Override
    public DeviceStateInfo getDeviceInfoById(String id) {
        return devicesInfo.get(id);
    }

    @Override
    public void addUpdateDeviceInfo(DeviceStateInfo deviceInfo) {
        devicesInfo.put(deviceInfo.getDeviceId(), deviceInfo);
    }

    @Override
    public void deleteDeviceInfo(String id) {
        devicesInfo.remove(id);
    }

}
