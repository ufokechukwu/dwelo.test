package com.francis.okechukwu.dwelotest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;
import com.francis.okechukwu.dwelotest.helpers.UIHelper;
import com.francis.okechukwu.dwelotest.interfaces.AdapterActionListener;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.device.Dimmer;
import com.francis.okechukwu.dwelotest.models.device.Lock;
import com.francis.okechukwu.dwelotest.models.device.Switch;
import com.francis.okechukwu.dwelotest.models.device.Thermostat;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ControlExpandListAdapter extends BaseExpandableListAdapter {
    private List<String> groups;
    private DevicesInfoDao devicesInfoDao;
    private DevicesDao devicesDao;
    private List<List<Device>>  groupedDevices;
    final public static String EXPAND = "EXPAND";
    final public static String CLICK_ACTION = "CLICK_ACTION";

    public ControlExpandListAdapter(List<String> groups) {
        this.groups = groups;
        this.devicesInfoDao = DAO.getDevicesInfoDaoInstance();
        this.devicesDao = DAO.getDevicesDaoInstance();
        this.groupedDevices = getGroupedDevicesInfo();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return groupedDevices.get(i).size();
    }

    @Override
    public Object getGroup(int i) {
        return groups.get(i);
    }

    @Override
    public Object getChild(int i, int childIndex) {
        return groupedDevices.get(i).get(childIndex);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int childIndex) {
        return childIndex;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String group = (String) getGroup(i);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.install_status_group_cell, null);
        }

        TextView stateNameTextView = view.findViewById(R.id.stateNameTextView);
        TextView stateDescriptionTextView = view.findViewById(R.id.stateDescriptionTextView);
        ImageView stateIconImageView = view.findViewById(R.id.stateIconImageView);

        if (group.equals(Device.Type.THERMO.toString())){
            stateNameTextView.setText("THERMOSTAT");
        }else{
            stateNameTextView.setText(group);
        }

        int childrenCount = getChildrenCount(i);
        stateDescriptionTextView.setText(childrenCount == 1? childrenCount + " installed device" : childrenCount + " installed devices");

        stateIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(group));
        action.run(view, i, -1, EXPAND, null);

        return view;
    }

    @Override
    public View getChildView(int i, int childIndex, boolean b, View convertView, ViewGroup viewGroup) {
        Device currentDevice = (Device) getChild(i, childIndex);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.install_status_item_cell, null);
        }

        TextView deviceNameTextView = convertView.findViewById(R.id.deviceNameTextView);
        TextView stateDateTextView = convertView.findViewById(R.id.stateDateTextView);
        TextView stateCommentTextView = convertView.findViewById(R.id.stateCommentTextView);
        ImageView deviceIconImageView = convertView.findViewById(R.id.deviceIconImageView);
        ImageButton switchStateImageButton = convertView.findViewById(R.id.switchStateImageButton);

        deviceNameTextView.setText(currentDevice.getName());
        DeviceStatus finalDeviceStatus = devicesInfoDao.getDeviceInfoById(currentDevice.getId()).getFinalStatus();
        String desc;
        if(currentDevice.getType() == Device.Type.SWITCH){
            desc = ((Switch) currentDevice).getState().toUpperCase();
        }else if(currentDevice.getType() == Device.Type.LOCK){
            desc = ((Lock) currentDevice).isLocked()? "LOCKED" : "OPENED";
        }else if(currentDevice.getType() == Device.Type.THERMO){
            desc = ((Thermostat) currentDevice).getTemp() + " 'F";
        }else{
            desc = ((Dimmer) currentDevice).getLevel() + "%";
        }

        stateDateTextView.setText(desc);
        stateCommentTextView.setVisibility(View.GONE);
        deviceIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(currentDevice));
        convertView.setOnClickListener(view -> action.run(view, i, childIndex, CLICK_ACTION, currentDevice.getId()));

        switchStateImageButton.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int childIndex) {
        return true;
    }


    // Set Custom Listeners...
    private AdapterActionListener action;
    public void setActionListener(AdapterActionListener action){
        this.action = action;
    }


    public List<List<Device>> getGroupedDevicesInfo(){
        HashMap<String, List<Device>> mTypeDeviceMap = new HashMap<>();

        List<Device> mAllDevices = this.devicesDao.getAllDevices();
        for (Device device: mAllDevices){
            String mFinalState = devicesInfoDao.getDeviceInfoById(device.getId()).getFinalStatus().getState().toString();
            if (mFinalState.equals(DeviceStatus.State.INSTALLED.toString())){
                String mType = device.getType().toString();
                List<Device> mTypeDeviceList = mTypeDeviceMap.get(mType) == null ? new ArrayList<>(): mTypeDeviceMap.get(mType);
                mTypeDeviceList.add(device);
                mTypeDeviceMap.put(mType, mTypeDeviceList);
            }
        }

        List<List<Device>> mappedDevicesList = new ArrayList<>();
        for (String group: groups){
            List<Device> mDevicesList = mTypeDeviceMap.get(group);
            mDevicesList = mDevicesList == null ? new ArrayList<>() : mDevicesList;
            mappedDevicesList.add(mDevicesList);
        }

        return mappedDevicesList;
    }
}
