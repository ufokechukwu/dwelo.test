package com.francis.okechukwu.dwelotest.ui.activities.device_status_detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.francis.okechukwu.dwelotest.adapters.StatusUpdateRecyclerAdapter;
import com.francis.okechukwu.dwelotest.constants.AppConstants;
import com.francis.okechukwu.dwelotest.databinding.ActivityDeviceStatusDetailBinding;
import com.francis.okechukwu.dwelotest.helpers.UIHelper;
import com.francis.okechukwu.dwelotest.interfaces.ViewSetupInterface;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;

public class DeviceStatusDetailActivity extends AppCompatActivity implements ViewSetupInterface {

    private ActivityDeviceStatusDetailBinding binding;
    private DeviceStatusDetailViewModel deviceStatusDetailViewModel;
    private StatusUpdateRecyclerAdapter statusUpdateRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDeviceStatusDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("PROCESS DETAILS");
        initViewSetupInterfaceMethods();    //initViewModels() -> initViews() -> initListeners() -> initViewLoaded()

    }

    @Override
    public void initViewModels() {
        deviceStatusDetailViewModel = new ViewModelProvider(this).get(DeviceStatusDetailViewModel.class);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String deviceId = extras.getString(AppConstants.DEVICE_ID);
            deviceStatusDetailViewModel.setDeviceId(deviceId);
        }
    }

    @Override
    public void initViews() {
        statusUpdateRecyclerAdapter = new StatusUpdateRecyclerAdapter(deviceStatusDetailViewModel.getDeviceId());
        binding.deviceStatusListRecyclerView.setAdapter(statusUpdateRecyclerAdapter);
        binding.deviceStatusListRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        Device currentDevice = deviceStatusDetailViewModel.getDevices().getDevice(deviceStatusDetailViewModel.getDeviceId());
        DeviceStateInfo currentDeviceInfo = deviceStatusDetailViewModel.getDevicesInfo().getDeviceInfoById(deviceStatusDetailViewModel.getDeviceId());
        binding.deviceNameTextView.setText(currentDevice.getName());
        binding.deviceTypeTextView.setText(currentDevice.getType().toString());
        binding.deviceIdTextView.setText(currentDevice.getId());
        binding.deviceTypeIconImageView.setImageResource(UIHelper.getDeviceTypeResourceId(currentDevice));
        binding.deviceStatusTextView.setText(currentDeviceInfo.getFinalStatus().getState().toString());

    }

    @Override
    public void initListeners() {
        binding.deviceDeleteMaterialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(DeviceStatusDetailActivity.this)
                        .setTitle("WARNING!")
                        .setMessage("You are about to delete this device. This action cannot be undone.")
                        .setCancelable(false)
                        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String deviceId = deviceStatusDetailViewModel.getDeviceId();
                                deviceStatusDetailViewModel.getDevicesInfo().deleteDeviceInfo(deviceId);
                                deviceStatusDetailViewModel.getDevices().deleteDevice(deviceId);
                                finish();
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void initViewLoaded() {

    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}