package com.francis.okechukwu.dwelotest.interfaces;

import android.view.View;

public interface AdapterActionListener {
    void run(View v, int groupPosition, int childPosition, String tag, Object data);
}
