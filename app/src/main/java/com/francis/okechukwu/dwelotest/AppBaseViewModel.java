package com.francis.okechukwu.dwelotest;

import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;

import androidx.lifecycle.ViewModel;

public class AppBaseViewModel extends ViewModel {
    private DevicesInfoDao devicesInfo = DAO.getDevicesInfoDaoInstance();
    private DevicesDao devices = DAO.getDevicesDaoInstance();
    private String deviceId;

    public DevicesInfoDao getDevicesInfo() {
        return devicesInfo;
    }

    public DevicesDao getDevices() {
        return devices;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
