package com.francis.okechukwu.dwelotest.models.device;

public class Device{
    private String id;
    private String name;
    private Type type;

    public Device(String id, String name, Type type) {
        this.id = id.toUpperCase();
        this.name = name;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setDeviceId(String id){
        this.id = id.toUpperCase();
    }


    public enum Type {
        THERMO {
            public String toString() {
                return "THERMO";
            }
        },
        DIMMER {
            public String toString() {
                return "DIMMER";
            }
        },
        LOCK {
            public String toString() {
                return "LOCK";
            }
        },
        SWITCH {
            public String toString() {
                return "SWITCH";
            }
        }
    }
}
