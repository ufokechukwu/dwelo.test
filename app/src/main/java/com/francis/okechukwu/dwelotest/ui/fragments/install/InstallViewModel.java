package com.francis.okechukwu.dwelotest.ui.fragments.install;

import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InstallViewModel extends ViewModel {

    private DevicesInfoDao devicesInfo = DAO.getDevicesInfoDaoInstance();
    private DevicesDao devices = DAO.getDevicesDaoInstance();

    public DevicesInfoDao getDevicesInfo() {
        return devicesInfo;
    }

    public DevicesDao getDevices() {
        return devices;
    }
}