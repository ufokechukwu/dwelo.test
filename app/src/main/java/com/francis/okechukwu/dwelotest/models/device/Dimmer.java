package com.francis.okechukwu.dwelotest.models.device;

import java.util.List;

public class Dimmer extends Device {
    private double level;

    public Dimmer(String id, String name, Double level) {
        super(id, name, Type.DIMMER);
        this.level = level;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }
}
