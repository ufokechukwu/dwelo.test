package com.francis.okechukwu.dwelotest.ui.fragments.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.adapters.ControlExpandListAdapter;
import com.francis.okechukwu.dwelotest.adapters.InstallExpandListAdapter;
import com.francis.okechukwu.dwelotest.constants.AppConstants;
import com.francis.okechukwu.dwelotest.databinding.FragmentDashboardBinding;
import com.francis.okechukwu.dwelotest.interfaces.AdapterActionListener;
import com.francis.okechukwu.dwelotest.interfaces.ViewSetupInterface;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;
import com.francis.okechukwu.dwelotest.ui.activities.create_edit_device.CreateEditDeviceActivity;
import com.francis.okechukwu.dwelotest.ui.activities.device_status_detail.DeviceStatusDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment implements ViewSetupInterface {

    private DashboardViewModel dashboardViewModel;
    private FragmentDashboardBinding binding;
    private ControlExpandListAdapter controlExpandListAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewSetupInterfaceMethods();    //initViewModels() -> initViews() -> initListeners() -> initViewLoaded()
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void initViewModels() {
        dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
    }

    @Override
    public void initViews() {
        reloadListView();
    }

    @Override
    public void initListeners() {
        setListViewListener();
    }

    @Override
    public void initViewLoaded() {

    }

    public void reloadListView(){
        List<String> groups = new ArrayList<>();
        groups.add(Device.Type.SWITCH.toString());
        groups.add(Device.Type.DIMMER.toString());
        groups.add(Device.Type.THERMO.toString());
        groups.add(Device.Type.LOCK.toString());
        controlExpandListAdapter = new ControlExpandListAdapter(groups);
        binding.statusExpandableListView.setAdapter(controlExpandListAdapter);
    }

    public void setListViewListener(){
        controlExpandListAdapter.setActionListener(new AdapterActionListener() {
            @Override
            public void run(View v, int groupPosition, int childPosition, String tag, Object data) {
                if (tag != null && tag.equals(InstallExpandListAdapter.EXPAND)){
                    binding.statusExpandableListView.expandGroup(groupPosition);
                }else if (tag != null && tag.equals(InstallExpandListAdapter.CLICK_ACTION)){

                    if (v.getId() == R.id.switchStateImageButton){
                        Intent intent = new Intent(getActivity(), CreateEditDeviceActivity.class);
                        intent.putExtra(AppConstants.CREATE_EDIT_ACTIVITY , AppConstants.EDIT);
                        String deviceId = (String) data;
                        intent.putExtra(AppConstants.DEVICE_ID , deviceId);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(getActivity(), DeviceStatusDetailActivity.class);
                        String deviceId = (String) data;
                        intent.putExtra(AppConstants.DEVICE_ID , deviceId);
                        startActivity(intent);
                    }
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (controlExpandListAdapter != null){
            reloadListView();
            setListViewListener();
        }
    }
}