package com.francis.okechukwu.dwelotest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.francis.okechukwu.dwelotest.R;
import com.francis.okechukwu.dwelotest.dao.DAO;
import com.francis.okechukwu.dwelotest.dao.DevicesDao;
import com.francis.okechukwu.dwelotest.dao.DevicesInfoDao;
import com.francis.okechukwu.dwelotest.helpers.UIHelper;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class StatusUpdateRecyclerAdapter extends RecyclerView.Adapter<StatusUpdateRecyclerAdapter.StatusUpdateViewHolder> {
    private DevicesInfoDao devicesInfoDao;
    private DevicesDao devicesDao;
    private String deviceId;

    public StatusUpdateRecyclerAdapter(String deviceId) {
        this.devicesInfoDao = DAO.getDevicesInfoDaoInstance();
        this.devicesDao = DAO.getDevicesDaoInstance();
        this.deviceId = deviceId;
    }

    @NonNull
    @Override
    public StatusUpdateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.status_update_item_cell, parent, false);
        return new StatusUpdateViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull StatusUpdateViewHolder holder, int position) {
        DeviceStatus deviceStatus = devicesInfoDao.getDeviceInfoById(deviceId).getDeviceStatuses().get(position);
        holder.cellCommentTextView.setText(deviceStatus.getComment());
        holder.cellDateTextView.setText(deviceStatus.getDate());
        holder.cellNameTextView.setText(deviceStatus.getState().toString());
        holder.cellStateIconImageView.setImageResource(UIHelper.getStateResourceId(deviceStatus.getState()));
    }

    @Override
    public int getItemCount() {
        return devicesInfoDao.getDeviceInfoById(deviceId).getDeviceStatuses().size();
    }

    public class StatusUpdateViewHolder extends RecyclerView.ViewHolder {

        public ImageView cellStateIconImageView;
        public TextView cellNameTextView;
        public TextView cellDateTextView;
        public TextView cellCommentTextView;


        public StatusUpdateViewHolder(@NonNull View itemView) {
            super(itemView);
            cellStateIconImageView = itemView.findViewById(R.id.cellStateIconImageView);
            cellNameTextView = itemView.findViewById(R.id.cellNameTextView);
            cellDateTextView = itemView.findViewById(R.id.cellDateTextView);
            cellCommentTextView = itemView.findViewById(R.id.cellCommentTextView);
        }
    }
}
