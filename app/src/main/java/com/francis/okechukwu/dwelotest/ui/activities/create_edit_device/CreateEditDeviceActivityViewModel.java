package com.francis.okechukwu.dwelotest.ui.activities.create_edit_device;


import com.francis.okechukwu.dwelotest.AppBaseViewModel;
import com.francis.okechukwu.dwelotest.constants.AppConstants;
import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.device.Dimmer;
import com.francis.okechukwu.dwelotest.models.device.Lock;
import com.francis.okechukwu.dwelotest.models.device.Switch;
import com.francis.okechukwu.dwelotest.models.device.Thermostat;

import java.util.Arrays;
import java.util.UUID;

public class CreateEditDeviceActivityViewModel extends AppBaseViewModel {
    private String viewType;
    public void setViewType(String viewType) {
        this.viewType = viewType;
    }


    public Boolean isEditMode() {
        return viewType.equals(AppConstants.EDIT);
    }

    public Boolean isCreateMode() {
        return viewType.equals(AppConstants.CREATE);
    }

    public Device createDefaultDeviceOfType(String type, String name) {
        if (type.equals(Device.Type.SWITCH.toString())) {
            return new Switch(UUID.randomUUID().toString(), name, "off");
        } else if (type.equals(Device.Type.DIMMER.toString())) {
            return new Dimmer(UUID.randomUUID().toString(), name, 50.0);
        } else if (type.equals(Device.Type.THERMO.toString())) {
            return new Thermostat(UUID.randomUUID().toString(), name, 70.0, Thermostat.Mode.AUTO);
        } else {
            //LOCK...
            return new Lock(UUID.randomUUID().toString(), name, Arrays.asList("1234"), false);
        }
    }
}
