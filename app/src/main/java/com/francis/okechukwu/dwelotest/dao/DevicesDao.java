package com.francis.okechukwu.dwelotest.dao;

import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.InstallGroup;

import java.util.List;

public interface DevicesDao {
    List<Device> getAllDevices();
    List<Device> getAllDevices(Device.Type type);
    Device getDevice(String id);
    void addUpdateDevice(Device device);
    void deleteDevice(String id);
}
