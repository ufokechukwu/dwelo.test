package com.francis.okechukwu.dwelotest.dao;

import android.util.Log;

import com.francis.okechukwu.dwelotest.models.device.Device;
import com.francis.okechukwu.dwelotest.models.device.Dimmer;
import com.francis.okechukwu.dwelotest.models.device.Lock;
import com.francis.okechukwu.dwelotest.models.device.Switch;
import com.francis.okechukwu.dwelotest.models.device.Thermostat;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStateInfo;
import com.francis.okechukwu.dwelotest.models.install_info.DeviceStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DAO {
    public static DevicesDao devicesDao = new DevicesDaoImpl();
    public static DevicesInfoDao devicesInfoDao = new DevicesInfoDaoImpl();
    public static Boolean defaultLoaded = false;

    public static DevicesDao getDevicesDaoInstance(){
        loadDefaultData();
        return devicesDao;
    }

    public static DevicesInfoDao getDevicesInfoDaoInstance(){
        loadDefaultData();
        return devicesInfoDao;
    }

    public static void loadDefaultData(){
        if (defaultLoaded){
            return;
        }

        Map<String, Object> dataMap = (new Gson()).fromJson(DataAPI.getAPIDataString(),
                new TypeToken<HashMap<String, Object>>() {}.getType());
        List<Map<String, Object>> devices = (List<Map<String, Object>>) dataMap.get("devices");

        for (Map<String, Object> device : devices){
            Device mDevice;
            if(device.get("type").equals(Device.Type.SWITCH.toString())){
                String mString = (new Gson()).toJson(device);
                mDevice = (new Gson()).fromJson(mString, Switch.class);
                mDevice.setDeviceId(UUID.randomUUID().toString());
                devicesDao.addUpdateDevice(mDevice);
            }else if(device.get("type").equals(Device.Type.LOCK.toString())){
                String mString = (new Gson()).toJson(device);
                mDevice = (new Gson()).fromJson(mString, Lock.class);
                mDevice.setDeviceId(UUID.randomUUID().toString());
                devicesDao.addUpdateDevice(mDevice);
            }else if(device.get("type").equals(Device.Type.DIMMER.toString())){
                String mString = (new Gson()).toJson(device);
                mDevice = (new Gson()).fromJson(mString, Dimmer.class);
                mDevice.setDeviceId(UUID.randomUUID().toString());
                devicesDao.addUpdateDevice(mDevice);
            }else if(device.get("type").equals(Device.Type.THERMO.toString())){
                String mString = (new Gson()).toJson(device);
                mDevice = (new Gson()).fromJson(mString, Thermostat.class);
                mDevice.setDeviceId(UUID.randomUUID().toString());
                devicesDao.addUpdateDevice(mDevice);
            }
        }

        int counter = 0;
        for (Device device: devicesDao.getAllDevices()){
            String comment = "Request was made for " + device.getType().toString() + " device. Entry has been submitted for further processing.";
            DeviceStatus deviceStatus = new DeviceStatus(comment, DeviceStatus.State.REQUESTED);
            List<DeviceStatus> statuses = new ArrayList<>(Arrays.asList(deviceStatus));
            if (counter != 4 && counter != 6 && counter != 1 && counter != 7){
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.PURCHASED));
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.SHIPPED));
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.INSTALLED));
            }else if (counter == 1){
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.PURCHASED));
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.SHIPPED));
            }else if (counter == 7){
                statuses.add(new DeviceStatus(comment, DeviceStatus.State.PURCHASED));
            }

            DeviceStateInfo deviceStateInfo = new DeviceStateInfo(device.getId(), statuses);
            devicesInfoDao.addUpdateDeviceInfo(deviceStateInfo);
            counter++;
        }


        defaultLoaded = true;
    }
}
