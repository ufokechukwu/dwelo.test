package com.francis.okechukwu.dwelotest.constants;

public class AppConstants {
    final public static String DEVICE_ID = "DEVICE_ID";
    final public static String CREATE_EDIT_ACTIVITY = "CREATE_EDIT_ACTIVITY";
    final public static String CREATE = "CREATE";
    final public static String EDIT = "EDIT";
}
