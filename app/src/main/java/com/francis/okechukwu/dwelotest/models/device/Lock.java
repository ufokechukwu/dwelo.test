package com.francis.okechukwu.dwelotest.models.device;

import java.util.ArrayList;
import java.util.List;

public class Lock extends Device {
    private List<String> codes = new ArrayList<>();
    private Boolean locked;

    public Lock(String id, String name, List<String> codes, Boolean locked) {
        super(id, name, Type.LOCK);
        this.codes = codes;
        this.locked = locked;
    }

    public Boolean isLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public void addCode(String code) {
        this.codes.add(code);
    }
}
